<?php get_header(); ?>

<div class="block text">
    <div class="grid-container">
        <div class="grid-x align-center text-center">
            <div class="large-6 cell">

                <!--    <?php echo the_search_query(); ?> -->
            </div>
        </div>

    </div>

</div>
<div class="block archivelist">

    <!-- MOBILE NIEUWSBLOK START -->
    <?php if (have_posts() && strlen(trim(get_search_query())) != 0) : while (have_posts()) : the_post(); ?>
            <div class="block nieuwsitem hide-for-large">
                <div class="grid-container full">
                    <div class="grid-x grid-margin-x">

                        <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                                else : echo 2;
                                                                endif; ?>" data-aos="fade-left">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                            <?php else : ?>
                                <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                            <?php endif; ?>
                        </div>
                        <div class="small-2 small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                        else : echo 1;
                                                        endif; ?> cell">
                            <!-- EMPTYCELL -->
                        </div>
                        <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                else : echo 4;
                                                                endif; ?>">
                            <div class="mobileinner">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>

                                <a href="<?php the_permalink(); ?>" class="arrowlink right">Lees meer <i class="icon-right"></i></a>
                            </div>
                        </div>
                        <div class="small-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                else : echo 3;
                                                                endif; ?>">
                            <!-- EMPTYCELL -->
                        </div>

                    </div>
                </div>
            </div>
            <!-- MOBILE NIEUWSBLOK END -->
            <!-- DESKTOP NIEUWSBLOK START -->
            <div class="block nieuwsitem show-for-large">
                <div class="grid-container fluid">
                    <div class="grid-x grid-margin-x">

                        <div class="large-1 small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                        else : echo 4;
                                                        endif; ?> cell">
                            <!-- EMPTYCELL -->
                        </div>
                        <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                else : echo 3;
                                                                endif; ?>" data-aos="fade-left">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                            <?php else : ?>
                                <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                            <?php endif; ?>
                        </div>
                        <div class="large-5 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                else : echo 2;
                                                                endif; ?>">
                            <div class="">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>

                                <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                            </div>
                        </div>
                        <div class="large-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                else : echo 1;
                                                                endif; ?>">
                            <!-- EMPTYCELL -->
                        </div>

                    </div>

                </div>
            </div>
            <!-- DESKTOP NIEUWSBLOK END -->

        <?php endwhile; ?>
    <?php else : ?>
        <!-- NO RESULTS -->
        <div class="block nieuwsitem show-for-large">
            <div class="grid-container fluid">
                <div class="grid-x grid-margin-x text-center align-center">
                    <div class="shrink cell">
                        <?php if (pll_current_language() == 'en') : ?>
                            <p>Unfortunately we were unable to find anything, please try another search term</p>
                        <?php else : ?>
                            <p>Helaas hebben wij niets kunnen vinden, probeer een andere zoekterm.</p>
                        <?php endif;  ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>



</div>


<?php
global $wp_query;
$total_results = $wp_query->found_posts;
?>

<?php
get_footer();

?>
