<?php /* Template Name: Doemee */ ?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="doemeepage ">
            <div class="grid-container full">
                <div class="grid-x">
                    <div class="small-12 medium-order-2 large-6 cell ">
                        <div class="formulier blockpadding ">
                            <div class="mobileinner">
                                <div class="grid-x align-center">
                                    <div class="large-8 cell text-center">
                                        <h2 class="breakword"><?php the_field('title_cover'); ?></h1>
                                            <?php $shortcode = get_field('shortcode'); ?>
                                            <?php if (!empty($shortcode)) : ?>

                                                <?php echo do_shortcode($shortcode) ?>
                                            <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small-12 medium-order-1 large-6 cell">
                        <?php $image = get_field('image');
                        $size = 'large'; ?>
                        <div class="fullheight" style="background: url('<?php echo wp_get_attachment_image_url($image, $size); ?>')no-repeat center center / cover;"></div>
                    </div>

                </div>
            </div>
        </div>
<?php endwhile;
endif; ?>

<?php get_footer(); ?>