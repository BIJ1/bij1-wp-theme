<!-- END OF container div -->
</div>

<div class="footer">
   <div class="footer-ontdek ">
      <div class="grid-container fluid">
         <div class="grid-x grid-margin-x">

            <div class="large-12 cell">
               <p class="nbm tm"><small><?php pll_e('Ontdek meer BIJ1'); ?></small></p>
               <hr class="fullwidthline">
            </div>

         </div>
         <div class="blockpadding">
            <div class="grid-x grid-margin-x align-center">

               <div class="large-4 cell">
                  <h1><?php the_field('about_titel', 'options'); ?></h1>
                  <p><?php the_field('about_text', 'options'); ?></p>
                  <div class="socials" role="social media links">
                     <div class="grid-x ">
                        <div class="shrink cell">
                           <?php
                           $link = get_field('twitter', 'options');
                           if(is_array($link)){
                               $link = $link['url'];
                           }
                           if ($link) : ?>
                              <a target="_blank" href="<?php echo esc_url($link); ?>" title="Twitter"><i class="icon-twitter"></i></a>
                           <?php endif; ?>
                        </div>
                        <div class="shrink cell">
                           <?php
                           $link = get_field('facebook', 'options');
                           if(is_array($link)){
                               $link = $link['url'];
                           }
                           if ($link) : ?>
                              <a target="_blank" href="<?php echo esc_url($link); ?>" title="facebook"><i class="icon-facebook-squared"></i></a>
                           <?php endif; ?>
                        </div>
                        <div class="shrink cell">
                           <?php
                           $link = get_field('instagram', 'options');
                           if(is_array($link)){
                               $link = $link['url'];
                           }
                           if ($link) : ?>
                              <a target="_blank" href="<?php echo esc_url($link); ?>" title="Instagram"><i class="icon-instagram"></i></a>
                           <?php endif; ?>
                        </div>
                        <div class="shrink cell">
                           <div class="shrink cell">
                              <?php
                              $link = get_field('linkedin', 'options');
                               if(is_array($link)){
                                   $link = $link['url'];
                               }
                              if ($link) : ?>
                                 <a target="_blank" href="<?php echo esc_url($link); ?>" title="Linkedin"><i class="icon-linkedin-squared"></i></a>
                              <?php endif; ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="large-4 cell" role="more navigation">
                  <div class="grid-x grid-margin-x grid-margin-y">
                     <div class="large-6 cell">
                        <h4 class="navtitle"><?php pll_e('Kom in actie'); ?>!</h4>
                        <?php
                        wp_nav_menu(array(
                           'theme_location'  => 'cta_menu',
                           'menu_class'      => 'menu vertical'
                        ));
                        ?>
                     </div>
                     <div class="large-6 cell">
                        <h4 class="navtitle"><?php pll_e('Over BIJ1'); ?></h4>
                        <?php
                        wp_nav_menu(array(
                           'theme_location'  => 'sec_menu',
                           'menu_class'      => 'menu vertical'
                        ));
                        ?>
                     </div>
                     <div class="large-6 cell">
                        <h4 class="navtitle"><?php pll_e('Informatie'); ?></h4>
                        <?php
                        wp_nav_menu(array(
                           'theme_location'  => 'info_menu',
                           'menu_class'      => 'menu vertical'
                        ));
                        ?>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>

   <div class="lowerfooter">
      <div class="full grid-container" role="being beautiful">

         <div class="grid-x border">
            <div class="small-2 cell">
               <div class="line orange"></div>
            </div>
            <div class="small-2 cell">
               <div class="line pink"></div>
            </div>
            <div class="small-2 cell">
               <div class="line green"></div>
            </div>
            <div class="small-2 cell">
               <div class="line purple"></div>
            </div>
            <div class="small-2 cell">
               <div class="line red"></div>
            </div>
            <div class="small-2 cell">
               <div class="line blue"></div>
            </div>
         </div>

      </div>
      <div class="grid-container fluid" role="design mention">
         <div class="lowerfooter-container">
            <div class="grid-x grid-margin-x align-middle">

               <div class="auto medium-shrink cell">
                  <div class="grid-x grid-margin-x">

                     <div class="small-12 medium-shrink cell">
                        <p class="nbm">Website <?php pll_e('door'); ?> <a href="https://www.multitude.nl/" target="_blank">Multitude</a></p>
                     </div>
                     <div class="small-12 medium-auto cell">
                        <p class="nbm">Identiteit <?php pll_e('door'); ?> <a href="https://www.burobraak.nl/" target="_blank">BUROBRAAK</a></p>
                     </div>

                  </div>
               </div>
               <div class="auto cell show-for-medium"></div>
               <div class="shrink cell">
                  <a class="footerlogo" href="<?php echo site_url(); ?>" title="home"><?php get_template_part('img/includes/logo.svg'); ?></a>
               </div>

            </div>
         </div>
      </div>
   </div>

</div>


<!-- SEARCHMENU -->
<div class="searchmenu" id="searchmenu" data-toggler=".expanded">
    <div class="flex-container flex-dir-column">

      <div class="flex-child-grow"></div>
      <div class="flex-child-shrink">
        <div class="grid-container fluid">

          <div class="grid-x align-center text-center">
            <div class="large-8 cell">

              <div class="altheader">
                <?php if (pll_current_language() == 'en') : ?>
                  <h4>What are you <span>looking</span> for </h4>
                <?php else : ?>
                  <h4>Waar wil je <span>meer</span><span>over</span><span>weten</span></h4>
                <?php endif;  ?>
              </div>

              <form id="searchform" method="get" action="<?php echo home_url('/'); ?>">
                <div class="grid-x align-middle search-bar">
                  <div class="auto cell">
                    <input id="searchfield" type="text" class="nbm" placeholder="Typ hier iets wat je zoekt" name="s" value="<?php the_search_query(); ?>">
                  </div>
                  <div class="shrink cell">
                    <button type="submit" class="third button nbm">
                      <?php pll_e('zoeken'); ?>
                    </button>
                  </div>
                </div>
              </form>


              <div class="grid-x grid-margin-x text-left">
                <div class="small-6 medium-4 cell">
                  <?php if (pll_current_language() == 'en') : ?>
                    <p style="display: block;">Themes</p>
                  <?php else : ?>
                    <p style="display: block;">Thema's</p>
                  <?php endif;  ?>
                  <?php
                  // Get the taxonomy's terms
                  $terms = get_terms(
                    array(
                      'taxonomy'   => 'thema',
                      'hide_empty' => true,
                    )
                  );

                  // Check if any term exists
                  if (!empty($terms) && is_array($terms)) {
                    // add links for each category
                    foreach ($terms as $term) { ?>
                      <p>
                        <small>
                          <a class="targetclass" href="<?php bloginfo('url') ?>/programma/?_themas=<?php echo $term->slug; ?>">
                            <?php echo $term->name; ?>

                          </a>
                      </p>
                      </small><?php
                            }
                          } ?>

                </div>
                <div class="medium-4 cell show-for-medium">
                  <p><?php pll_e('Verkiezingen'); ?></p>
                  <?php if (have_rows('pagina_links', 'options')) : ?>

                    <?php while (have_rows('pagina_links', 'options')) : the_row(); ?>
                      <?php
                      $link = get_sub_field('pagina_link', 'options');
                      if ($link) :
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                      ?>
                        <p> <small><a class="" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></small></p>
                      <?php endif; ?>
                    <?php endwhile; ?>

                  <?php endif; ?>
                </div>
                <div class="small-6 medium-4 cell ">
                  <p><?php pll_e('Nieuws'); ?></p>
                  <?php $query = new WP_Query(array('posts_per_page' => 4)); ?>
                  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                      <p><small><a href="<?php the_permalink(); ?>" class=""><?php the_title(); ?></a></small></p>

                  <?php endwhile;
                    wp_reset_postdata();
                  endif; ?>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>
      <div class="flex-child-grow"></div>
      <a data-toggle="searchmenu" id="searchmenuclosebutton"><span></span><span></span></a>
    </div>
  </div>
  <!-- SEARCHMENU END -->


<?php wp_footer(); ?>

<script>


$( ".search-button" ).click(function() {
  $( "#searchfield" ).focus();
});

$( "#menu-toggle" ).click(function() {
   $( this ).attr("aria-expanded","true");
});

</script>

<!-- CONTAINER CLOSED-->
</div>

</body>

</html>
