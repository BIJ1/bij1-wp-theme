<!-- DESKTOP NIEUWSBLOK START -->
<?php if ($alm_current == 1) : ?>

    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'fullwidth'); ?>


    <?php if (has_post_thumbnail()) : ?>
        <div class="imageblock" style="background: url('<?php echo $featured_img_url ?>')no-repeat center center / cover;">
        <?php else : ?>
            <div class="imageblock" style="background: url('<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png')no-repeat center center / cover;">
            <?php endif; ?>




            <div class="inner">
                <div class="headercontainer">
                    <div class="grid-x align-center text-center">
                        <div class="small-10 large-10 cell">

                            <?php if (!empty(get_field('post_title', get_the_ID()))) : ?>
                                <h1> <?php the_field('post_title', get_the_ID()); ?></h1>
                            <?php else : ?>
                                <h1><?php the_title(); ?></h1>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <a href="<?php the_permalink(); ?>" class="button whitebutton nbm">Lees meer</a>
            </div>
            </div>

        <?php elseif ($alm_current == 2 or $alm_current == 3) : ?>
            <!-- MOBILE NIEUWSBLOK START -->
            <div class="block nieuwsitem hide-for-large">
                <div class="grid-container full">
                    <div class="grid-x grid-margin-x">

                        <div class="small-10 cell small-order-<?php if ($alm_current % 2 == 1) : echo 1;
                                                                else : echo 2;
                                                                endif; ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                            <?php else : ?>
                                <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                            <?php endif; ?>
                        </div>

                        <div class="small-2 small-order-<?php if ($alm_current % 2 == 1) : echo 2;
                                                        else : echo 1;
                                                        endif; ?> cell">
                            <!-- EMPTYCELL -->
                        </div>

                        <div class="small-10 cell small-order-<?php if ($alm_current % 2 == 1) : echo 3;
                                                                else : echo 4;
                                                                endif; ?>">
                            <div class="mobileinner">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>

                                <a href="<?php the_permalink(); ?>" class="arrowlink right">Lees meer <i class="icon-right"></i></a>
                            </div>
                        </div>

                        <div class="small-2 cell small-order-<?php if ($alm_current % 2 == 1) : echo 4;
                                                                else : echo 3;
                                                                endif; ?>">
                            <!-- EMPTYCELL -->
                        </div>

                    </div>
                </div>
            </div>
            <!-- MOBILE NIEUWSBLOK END -->
            <!-- LOOP FOR MEDIUM UP START -->
            <div class="block nieuwsitem show-for-medium">
                <div class="grid-container fluid">
                    <div class="grid-x grid-margin-x">

                        <div class="large-1 small-order-<?php if ($alm_current % 2 == 1) : echo 1;
                                                        else : echo 4;
                                                        endif; ?> cell">
                            <!-- EMPTYCELL -->
                        </div>

                        <div class="large-4 cell small-order-<?php if ($alm_current % 2 == 1) : echo 2;
                                                                else : echo 3;
                                                                endif; ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('fullwidth', array('class' => 'nieuwsimage')); ?>
                            <?php else : ?>
                                <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                            <?php endif; ?>
                        </div>

                        <div class="large-5 cell small-order-<?php if ($alm_current % 2 == 1) : echo 3;
                                                                else : echo 2;
                                                                endif; ?>">
                            <div class="">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="arrowlink right">Lees meer <i class="icon-right"></i></a>
                            </div>
                        </div>

                        <div class="large-2 cell small-order-<?php if ($alm_current % 2 == 1) : echo 4;
                                                                else : echo 1;
                                                                endif; ?>">
                            <!-- EMPTYCELL -->
                        </div>

                    </div>
                </div>
            </div>
            <!-- LOOP FOR MEDIUM UP END -->






        <?php else : ?>

            <?php if ($alm_current == 4) : ?>
                <div class="block nieuwsitem ">
                    <div class="grid-container fluid">
                        <div class="grid-x grid-margin-x">
                        <?php endif; ?>

                        <div class="small-6 large-3 cell">
                            <div class="imagecontainer-small">
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('large'); ?>
                                <?php else : ?>
                                    <img src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                                <?php endif; ?>

                            </div>
                            <div class="flex-container flex-dir-column subscript">
                                <div class="flex-child-grow">
                                    <p class="bold"><?php the_title(); ?></p>
                                </div>
                                <div class="flex-child-shrink">
                                    <a href="<?php the_permalink(); ?>" class="arrowlink right">Lees meer <i class="icon-right"></i></a>
                                </div>
                            </div>

                        </div>

                        <?php if ($alm_current == 8) : ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        <?php endif; ?>