<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


      <?php if (is_singular('kandidaten')) : ?>

         <div class="block socials" data-aos="fade-up">
            <div class="grid-container">
               <div class="grid-x grid-margin-x text-center align-center">
                  <div class="large-12 cell">
                     <?php if (pll_current_language() == 'en') : ?>
                        <h4>Follow <?php the_title(); ?> on</h4>
                     <?php else : ?>
                        <h4>Volg <?php the_title(); ?> op</h4>
                     <?php endif;  ?>
                  </div>
                  <div class="shrink cell">
                     <?php if (get_field('twitter')) : ?>
                        <a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('twitter')); ?>"><i class="icon-twitter"></i></a>
                     <?php endif; ?>
                  </div>
                  <div class="shrink cell">
                     <?php if (get_field('instagram')) : ?>
                        <a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('instagram')); ?>"><i class="icon-instagram"></i></a>
                     <?php endif; ?>
                  </div>
                  <div class="shrink cell">
                     <?php if (get_field('facebook')) : ?>
                        <a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('facebook')); ?>"><i class="icon-facebook"></i></a>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>

      <?php endif; ?>

      <!-- THE CONTENT -->
      <?php the_content(); ?>
      <!-- THE CONTENT -->

      <?php if (is_singular('programma')) : ?>
         <div class="navfooter lastblock">
            <div class="grid-container full">

               <div class="grid-x text-center ">
                  <div class="large-6 cell">
                     <div class="background blackbackground">
                        <a href="/programma" class="arrowlink left" href=""><i class="icon-left"></i><?php pll_e('Terug naar het programma overzicht'); ?></a>
                     </div>
                  </div>
                  <div class="large-6 cell">
                     <div class="background yellowbackground">
                        <?php if (pll_current_language() == 'en') : ?>to<?php else : ?>Naar<?php endif;  ?>
                        <?php $next = get_previous_post(); ?>
                        <?php $previous = get_next_post(); ?>
                        <?php if (!empty($next)) : ?>
                           <a href="<?php echo $next->guid; ?>" class=" arrowlink right" href=""> <?php if (pll_current_language() == 'en') : ?>to<?php else : ?>Naar<?php endif;  ?>
                              <?php echo $next->post_title; ?><i class="icon-right"></i></a>
                        <?php else : ?>
                           <a href="<?php echo $previous->guid; ?>" class=" arrowlink right" href=""> <?php if (pll_current_language() == 'en') : ?>to<?php else : ?>Naar<?php endif;  ?>
                              <?php echo $previous->post_title; ?><i class="icon-right"></i></a>
                        <?php endif; ?>

                     </div>
                  </div>
               </div>

            </div>
         </div>

      <?php elseif (is_singular('kandidaten')) : ?>


         <div class="navfooter lastblock">
            <div class="grid-container full">

               <div class="grid-x text-center ">
                  <div class="large-6 cell">
                     <div class="background blackbackground">
                        <a href="/kandidaten/" class="arrowlink left" href=""><i class="icon-left"></i><?php pll_e('Terug naar het kandidaten overzicht'); ?></a>
                     </div>
                  </div>
                  <div class="large-6 cell">
                     <div class="background yellowbackground">
                        <?php $previous = get_next_post(); ?>
                        <?php $next = get_previous_post(); ?>

                        <?php if (!empty($next)) : ?>

                           <a href="<?php echo $next->guid; ?>" class=" arrowlink right" href=""> <?php if (pll_current_language() == 'en') : ?>to<?php else : ?>Naar<?php endif;  ?>
                              <?php echo $next->post_title; ?><i class="icon-right"></i></a>
                        <?php else : ?>

                           <a href="<?php echo $previous->guid; ?>" class=" arrowlink right" href=""> <?php if (pll_current_language() == 'en') : ?>to<?php else : ?>Naar<?php endif;  ?>
                              <?php echo $previous->post_title; ?><i class="icon-right"></i></a>
                        <?php endif; ?>

                     </div>
                  </div>
               </div>

            </div>
         </div>

      <?php elseif (is_singular('post')) : ?>
         <!-- ELSE = NIEUWS -->
         <!-- DIT BLOK IS EEN COPIE VAN NIEUWS_ITEMS.PHP -->

         <!-- NIEUWSBLOCK START -->
         <div class="nieuws-items xltm">

            <div class='titelblock'>
               <div class='grid-container fluid'>

                  <div class='grid-x grid-margin-x'>
                     <div class='large-12 cell'>
                        <hr class="fullwidthline">
                     </div>
                  </div>

                  <div class="blockpaddingtop">
                     <div class="grid-x grid-margin-x align-center text-center">
                        <div class="large-8 cell">
                           <div class="altheader">
                              <h1>
                                 <?php if (pll_current_language() == 'en') : ?>Related articles<?php else : ?>Gerelateerde artikelen<?php endif;  ?>
                              </h1>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>



            <?php $query = new WP_Query(array(
               'posts_per_page' => 3,
               'post__not_in' => array(get_the_ID())
            )); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                  <div class="block nieuwsitem">
                     <div class="grid-container fluid">
                        <div class="grid-x grid-margin-x">

                           <div class="large-1 small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                            else : echo 4;
                                                            endif; ?> cell">
                              <!-- EMPTYCELL -->
                           </div>
                           <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                  else : echo 3;
                                                                  endif; ?>" data-aos="fade-<?php if ($query->current_post % 2 == 1) : echo 'right';
                                                                                             else : echo 'left';
                                                                                             endif; ?>">
                              <?php the_post_thumbnail('large'); ?>
                           </div>
                           <div class=" large-5 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                  else : echo 2;
                                                                  endif; ?>">
                              <div class="">
                                 <h3><?php the_title(); ?></h3>
                                 <p><?php the_excerpt(); ?></p>

                                 <a href="<?php the_permalink(); ?>" class="arrowlink"><?php pll_e('Lees meer'); ?></a>
                              </div>
                           </div>
                           <div class="large-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                  else : echo 1;
                                                                  endif; ?>">
                              <!-- EMPTYCELL -->
                           </div>

                        </div>
                     </div>
                  </div>

            <?php endwhile;
               wp_reset_postdata();
            endif; ?>

            <div class="navfooter lastblock">
               <div class="grid-container full">
                  <div class="grid-x  text-center align-middle">
                     <div class="large-12 cell">
                        <div class="background blackbackground">
                           <a href="/nieuws" class="arrowlink right"><?php pll_e('Naar het nieuwsoverzicht'); ?> <i class="icon-right"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>


         </div>
         <!-- NIEUWSBLOCK EINDE -->
      <?php else : ?>
         <div class="spacingblock"></div>
      <?php endif; ?>

      <?php get_template_part('parts/blocks/cta_blocks') ?>



<?php endwhile;
endif; ?>

<?php get_footer(); ?>
