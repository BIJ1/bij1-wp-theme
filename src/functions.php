<?php

// try setting upload limits...
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
// end upload limits

/* -- REGISTER SCRIPTS
   ------------------------------- */

if ( ! is_admin() ) {
    function my_load_scripts($hook) {
        wp_deregister_script('jquery');

        $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/js/app.js' ));
        $my_css_ver = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/css/app.css' ));

        wp_enqueue_script( 'jquery', get_template_directory_uri().'/js/jquery.min.js', '', 1, true );
        wp_enqueue_script( 'what-input', get_template_directory_uri().'/js/what-input.min.js', '', 1, true );
        wp_enqueue_script( 'foundation', get_template_directory_uri().'/js/foundation.min.js', '', 1, true );

        wp_enqueue_script('aos', get_template_directory_uri() . '/js/aos.js', '', 1, true);
        wp_enqueue_script( 'app', get_template_directory_uri().'/js/app.js', '', $my_js_ver, true );

        wp_enqueue_style('fontello',  get_template_directory_uri() . '/css/fontello.css');
        wp_enqueue_style('aos',  get_template_directory_uri() . '/css/aos.css');
        wp_enqueue_style('base_css',  get_template_directory_uri().'/css/base.css' );
        wp_enqueue_style('app_css',  get_template_directory_uri().'/css/app.css', '', '1.0');
    }
    
    add_action('wp_enqueue_scripts', 'my_load_scripts');
}

/* -- BASICS
------------------------------- */



// Force Gravity Forms to init scripts in the footer and ensure that the DOM is loaded before scripts are executed.
add_filter( 'gform_init_scripts_footer', '__return_true' );
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open', 1 );
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close', 99 );

function wrap_gform_cdata_open( $content = '' ) {
    if ( ! do_wrap_gform_cdata() ) {
        return $content;
    }
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ' . $content;
    return $content;
}

function wrap_gform_cdata_close( $content = '' ) {
    if ( ! do_wrap_gform_cdata() ) {
        return $content;
    }
    $content .= ' }, false );';
    return $content;
}

function do_wrap_gform_cdata() {
    if (
        is_admin()
        || ( defined( 'DOING_AJAX' ) && DOING_AJAX )
        || isset( $_POST['gform_ajax'] )
        || isset( $_GET['gf_page'] ) // Admin page (eg. form preview).
        || doing_action( 'wp_footer' )
        || did_action( 'wp_footer' )
    ) {
        return false;
    }
    return true;
}

function isMainSite(){
    return defined('IS_MAIN_SITE') && IS_MAIN_SITE;
}

function logoBigPath(){
    if(defined('LOGO_BIG_PATH')){
        return LOGO_BIG_PATH;
    } else {
        return '/wp-content/themes/BIJ1/img/logo.svg';
    }
}

function logoSmallPath(){
    if(defined('LOGO_SMALL_PATH')){
        return LOGO_SMALL_PATH;
    } else {
        return '/wp-content/themes/BIJ1/img/logo.svg';
    }
}

function useNewsGridView() {
    if(defined('NEWS_GRID_VIEW')){
        return NEWS_GRID_VIEW;
    } else {
        return false;
    }
}


require_once(get_template_directory() . '/functions/guthenberg.php');

/* REGISTER NAV MENU'S */
register_nav_menus( array(
  'header' => 'Header menu',
  'main' => 'Main menu',
    'sec_menu' => 'Secondary menu',
  'cta_menu' => 'Cta menu',
  'language_menu' => 'Language menu',
  'info_menu' => 'Info menu'
) );


/* ADD THUMBNAILS TO POSTS */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'fullwidth', 1920, 1080, true );
    add_image_size( 'square', 1080, 1080, true );
}


/* ADD FOUNDATION DIV TO VIDEO'S */
add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);
function wrap_embed_with_div($html, $url, $attr) {
    return '<div class="responsive-embed widescreen">'.$html.'</div>';
}


/* ALWAYS SHOW EXREPT IN ADMIN */
function wpse_edit_post_show_excerpt( $user_login, $user ) {
    $unchecked = get_user_meta( $user->ID, 'metaboxhidden_post', true );
    $key = array_search( 'postexcerpt', $unchecked );
    if ( FALSE !== $key ) {
        array_splice( $unchecked, $key, 1 );
        update_user_meta( $user->ID, 'metaboxhidden_post', $unchecked );
    }
}
add_action( 'wp_login', 'wpse_edit_post_show_excerpt', 10, 2 );


/* REMOVE USELESS MENU ITEMS */
function remove_menus() {
    remove_menu_page( 'jetpack' );                    //Jetpack
    remove_menu_page( 'edit-comments.php' );          //Comments
    remove_menu_page( 'link-manager.php' );           //Links
    remove_menu_page( 'tools.php' );                  //Tools
}
add_action( 'admin_menu', 'remove_menus' );



/* -- SHORTCODE WOORDENBOEK
------------------------------- */

function dictionary_function($atts = array(), $content = null)
{

  extract(shortcode_atts(array(
    'title' => '',
    'side' => 'left'
  ), $atts));

  return '<span class="purpletext">' . $title . '</span> <i class="icon-' . $side . ' purpleicon"></i><span class="dictionary-panel ' . $side . '">
    <span class="dic-title">' . $title . '</span>' . $content . '</span>';
}

add_shortcode('dictionary', 'dictionary_function');



/* -- OPTIONAL
------------------------------- */

/* -- ADD ACF OPTIONS */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}


/* -- ADD EXCERPTS TO PAGES
add_post_type_support( 'page', 'excerpt' );


/* -- REMOVE TAGS FROM POSTS  */
function howling_unregister_tags() {
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'howling_unregister_tags' );


/* -- REMOVE CATEGORIES FROM POSTS
function howling_unregister_categories() {
    unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action( 'init', 'howling_unregister_categories' );


/* -- HIDE ADMIN BAR
add_filter(‘show_admin_bar’, ‘__return_false’);



/* -- CUSTOM POST TYPES
------------------------------- */

function codex_custom_init() {

    $args = array(
      'public' => true,
      'label'  => 'Programma',
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'show_in_rest' => true
    );
    register_post_type( 'programma', $args );

    $args2 = array(
      'public' => true,
      'label'  => 'Kandidaten',
      'supports' => array( 'title', 'editor','thumbnail', 'excerpt' ),
      'show_in_rest' => true,
      'order', 'DESC'
    );
    register_post_type( 'kandidaten', $args2 );

    $args3 = array(
      'public' => true,
      'label'  => 'Afdelingen',
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
      'show_in_rest' => true
    );
    register_post_type( 'afdelingen', $args3 );

    $args4 = array(
      'public' => true,
      'label'  => 'Vacatures',
      'supports' => array( 'title', 'editor' ),
      'show_in_rest' => true
    );
    register_post_type( 'vacatures', $args4 );

}
add_action( 'init', 'codex_custom_init' );


/* CUSTOM TAXOMOMIES */
function custom_taxonomy()
{

  $labels = array(
    'name' => _x('Thema', 'Taxonomy General Name', 'text_domain'),
  );

  $args2 = array(
    'labels' => $labels,
    'hierarchical' => true,
    'public' => true,
    'show_in_rest' => true
  );
  
  register_taxonomy('thema', array('programma'), $args2);


  $labels = array(
    'name' => _x('Soort', 'Taxonomy General Name', 'text_domain'),
  );
  $args4 = array(
    'labels' => $labels,
    'hierarchical' => true,
    'public' => true,
    'show_in_rest' => true
  );
  register_taxonomy('soort', array('vacatures'), $args4);
}

add_action('init', 'custom_taxonomy', 0);

function get_vacature_terms(){
    global $wpdb;

    $query = $wpdb->prepare("
        SELECT  t.*, tt.* FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('soort') ORDER BY t.name ASC
    ");

    $results = $wpdb->get_results($query);
    return $results;
}


/* SEARCH INPUT EMPTY RETURN NOT FOUND */

function SearchFilter($query)
{
  // If 's' request variable is set but empty
  if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()) {
    $query->is_search = true;
    $query->is_home = false;
  }
  return $query;
}
add_filter('pre_get_posts', 'SearchFilter');


/* -- STRING TRANSLATIONS
------------------------------- */

if (function_exists('pll_register_string')) {
    add_action('init', function () {

      pll_register_string('Naar het programma', 'Naar het programma');
      pll_register_string('Naar het nieuwsoverzicht', 'Naar het nieuwsoverzicht');
      pll_register_string('Naar het volledige programma', 'Naar het volledige programma');
      pll_register_string('Terug naar het kandidaten overzicht', 'Terug naar het kandidaten overzicht');
      pll_register_string('Terug naar het programma overzicht', 'Terug naar het programma overzicht');
      pll_register_string('Lees meer', 'Lees meer');
      pll_register_string('Nieuws', 'Nieuws');
      pll_register_string('Programma', 'Programma');
      pll_register_string('Verkiezingen', 'Verkiezingen');
      pll_register_string('Naar de site', 'Naar de site');
      pll_register_string('Mail ons', 'Mail ons');
      pll_register_string('Doneer', 'Doneer');
      pll_register_string('Nieuwsbrief', 'Nieuwsbrief');
      pll_register_string('Lees de introductie', 'Lees de introductie');
      pll_register_string('Terug', 'Terug');
      pll_register_string('Inhoudsopgave', 'Inhoudsopgave');
      pll_register_string('Kandidaten', 'Kandidaten');
      pll_register_string('Kandidaten overzicht', 'Kandidaten overzicht');
      pll_register_string('is al gedoneerd', 'is al gedoneerd');
      pll_register_string('Fundraiser wordt geladen', 'Fundraiser wordt geladen');
      pll_register_string('Ja ik help mee voor', 'Ja ik help mee voor');
      pll_register_string('Blijf op de hoogte', 'Blijf op de hoogte');
      pll_register_string('Ik ga akkoord met het', 'Ik ga akkoord met het');
      pll_register_string('privacybeleid', 'privacybeleid');
      pll_register_string('Ontdek meer BIJ1', 'Ontdek meer BIJ1');
      pll_register_string('Kom in actie', 'Kom in actie');
      pll_register_string('Over BIJ1', 'Over BIJ1');
      pll_register_string('Informatie', 'Informatie');
      pll_register_string('door', 'door');
      pll_register_string('zoeken', 'zoeken');
      pll_register_string('Ben jij BIJ1', 'Ben jij BIJ1');
      pll_register_string('Introductie', 'Introductie');
    });
}

if(!function_exists('pll_e')) {
    function pll_e($str){
        echo $str;
    }
}

if(!function_exists('pll_current_language')) {
    function pll_current_language(){
        return 'nl';
    }
}

function isNl() {
    return(
        function_exists('pll_current_language')
        && pll_current_language() == 'nl'
    );
}

add_filter('gform_address_display_format', 'address_format');
function address_format($format)
{
  return 'zip_before_city';
}

// Disable big image threshold
add_filter( 'big_image_size_threshold', '__return_false' );

// Include ACF field settings
require_once('acf.php');

// Allow editors to see Appearance menu
$role_object = get_role('editor');
$role_object->add_cap('edit_theme_options');
function hide_menu() {
    // Hide theme selection page
    remove_submenu_page('themes.php', 'themes.php');

    // Hide widgets page
    remove_submenu_page('themes.php', 'widgets.php');

    // Hide customize page
    global $submenu;
    unset($submenu['themes.php'][6]);
}

add_action('admin_head', 'hide_menu');

/* more helpers */
function bij1_post_published() {
	$published_date = get_post_datetime(get_the_ID());
	if ($published_date) {
		printf("<h5 class=\"date\">%s</h5>", date_format($published_date, "d-m-Y"));
	}
}


