<!-- NIEUWSBLOCK START -->
<div class="nieuws-items">

    <?php if (is_page('nieuws')) : ?>
        <?php if(useNewsGridView()): ?>
            <!-- LOADMORE START -->
            <?php echo do_shortcode('[ajax_load_more post_type="post" button_label="Laad meer nieuws" transition_container="false" scroll="false" posts_per_page="8" ]'); ?>
            <!-- LOADMORE END -->
        <?php else: ?>
            <!-- LOADMORE START -->
            <?php echo do_shortcode('[ajax_load_more post_type="post" button_label="Laad meer nieuws" transition_container="false" scroll="false" posts_per_page="7" ]'); ?>
            <!-- LOADMORE END -->
        <?php endif; ?>
    <?php else : ?>
        <div class='titelblock'>
            <div class='grid-container fluid'>
                <div class='grid-x grid-margin-x'>
                    <div class='large-12 cell'>

                        <p class="nbm tm"><small><?php pll_e('Nieuws'); ?></small></p>
                        <hr class="fullwidthline">

                    </div>
                </div>
                <div class="blockpaddingtop">
                    <div class="grid-x grid-margin-x align-center text-center" data-aos="fade-up">
                        <div class="auto cell"></div>
                        <div class="large-8 cell">
                            <div class="altheader">
                                <h1><?php the_field('title_nieuws'); ?></h1>
                            </div>
                        </div>
                        <div class="auto cell"></div>
                        <div class="large-12 cell">
                            <a class="arrowlink right" href="/nieuws"><?php pll_e('Naar het nieuwsoverzicht'); ?> <i class="icon-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?php if (is_front_page()) :
            $postnumber = 4;
        else :
            $postnumber = -1;
        endif; ?>

        <?php $num = 0; ?>
        <?php $query = new WP_Query(array('posts_per_page' => $postnumber)); ?>
        <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                <?php if ($num == 0) : ?>

                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                    <div class="imageblock" style="background: url('<?php echo $featured_img_url ?>')no-repeat center center / cover;">
                        <div class="inner">
                            <div class="headercontainer">
                                <div class="grid-x align-center text-center">
                                    <div class="small-10 large-10 cell">


                                        <?php if (!empty(get_field('post_title', get_the_ID()))) : ?>
                                            <h1> <?php the_field('post_title', get_the_ID()); ?></h1>
                                        <?php else : ?>
                                            <h1><?php the_title(); ?></h1>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="button whitebutton nbm"><?php pll_e('Lees meer'); ?></a>
                        </div>
                    </div>

                <?php else : ?>
                    <!-- MOBILE NIEUWSBLOK START -->
                    <div class="block nieuwsitem hide-for-large">
                        <div class="grid-container full">
                            <div class="grid-x grid-margin-x">

                                <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                                        else : echo 2;
                                                                        endif; ?>" data-aos="fade-<?php if ($query->current_post % 2 == 1) : echo 'right';
                                                                                                    else : echo 'left';
                                                                                                    endif; ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                                    <?php else : ?>
                                        <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                                    <?php endif; ?>
                                </div>
                                <div class="small-2 small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                else : echo 1;
                                                                endif; ?> cell">
                                    <!-- EMPTYCELL -->
                                </div>
                                <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                        else : echo 4;
                                                                        endif; ?>">
                                    <div class="mobileinner">
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_excerpt(); ?></p>

                                        <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                    </div>
                                </div>
                                <div class="small-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                        else : echo 3;
                                                                        endif; ?>">
                                    <!-- EMPTYCELL -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- MOBILE NIEUWSBLOK END -->


                    <!-- DESKTOP NIEUWSBLOK START -->
                    <div class="block nieuwsitem show-for-large">
                        <div class="grid-container fluid">
                            <div class="grid-x grid-margin-x">

                                <div class="large-1 small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                                else : echo 4;
                                                                endif; ?> cell">
                                    <!-- EMPTYCELL -->
                                </div>
                                <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                        else : echo 3;
                                                                        endif; ?>" data-aos="fade-<?php if ($query->current_post % 2 == 1) : echo 'right';
                                                                                                    else : echo 'left';
                                                                                                    endif; ?>">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                                    <?php else : ?>
                                        <img class="nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                                    <?php endif; ?>
                                </div>
                                <div class="large-5 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                        else : echo 2;
                                                                        endif; ?>">
                                    <div class="">
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php the_excerpt(); ?></p>

                                        <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                    </div>
                                </div>
                                <div class="large-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                        else : echo 1;
                                                                        endif; ?>">
                                    <!-- EMPTYCELL -->
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- DESKTOP NIEUWSBLOK END -->

                <?php endif; ?>

                <?php $num++ ?>

        <?php endwhile;
            wp_reset_postdata();
        endif; ?>


        <div class="navfooter">
            <div class="grid-container full">
                <div class="grid-x  text-center align-middle">
                    <div class="large-12 cell">
                        <div class="background blackbackground">
                            <a href="/nieuws" class="arrowlink right"><?php pll_e('Naar het nieuwsoverzicht'); ?><i class="icon-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <?php endif; ?>
</div>
<!-- NIEUWSBLOCK EINDE -->
