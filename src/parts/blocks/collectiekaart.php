<?php
$featured_posts = get_field('kandidaat');
if ($featured_posts) : ?>

    <div class="block collectiekaart">
        <div class="grid-container fluid">
            <div class="grid-x align-center">

                <?php foreach ($featured_posts as $featured_post) :
                    $thumbnail_id = get_post_thumbnail_id($featured_post->ID);

                    $permalink = get_permalink($featured_post->ID);
                    $title = get_the_title($featured_post->ID);

                ?>

                    <div class="large-8 cell">


                        <div class="inner">
                            <div class="grid-x grid-margin-x align-middle">
                                <div class="large-4 cell">

                                    <?php
                                    if ($thumbnail_id) {
                                        echo wp_get_attachment_image($thumbnail_id, 'square', "", array("class" => "square_image"));
                                    }
                                    ?>
                                </div>
                                <div class="large-8 cell">
                                    <a href="<?php the_permalink($featured_post->ID); ?>">
                                        <h3><?php echo get_the_title($featured_post->ID); ?></h3>
                                        <p><?php the_excerpt($featured_post->ID) ?></p>
                                    </a>

                                    <div class="grid-x grid-margin-x ">
                                        <!-- HIER MOETEN NOG  portefeuilles -->
                                        <div class="shrink cell">
                                            <?php if (get_field('twitter', $featured_post->ID)) : ?>
                                                <div class="socials"><a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('twitter', $featured_post->ID)); ?>"><i class="icon-twitter"></i></a></div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="shrink cell">
                                            <?php if (get_field('instagram', $featured_post->ID)) : ?>
                                                <div class="socials"><a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('facebook')); ?>"><i class="icon-instagram"></i></a></div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="shrink cell">
                                            <?php if (get_field('facebook', $featured_post->ID)) : ?>
                                                <div class="socials"><a class="button whitebutton roundbutton" target="_blank" href="<?php echo esc_url(get_field('facebook')); ?>"><i class="icon-facebook"></i></a></div>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                    <a href="<?php the_permalink($featured_post->ID); ?>" class="arrowlink right">
                                        <?php pll_e('Lees meer'); ?> <i class="icon-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>

<?php endif; ?>