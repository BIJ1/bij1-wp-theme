<!-- PROGRAMMEBLOCK START -->
<div class="programma_items">

    <div class='titelblock'>
        <div class='grid-container fluid'>
            <div class='grid-x grid-margin-x'>
                <div class='large-12 cell'>
                    <?php if (is_front_page()) : ?>
                        <p class="nbm tm"><small><?php pll_e('Programma'); ?></small></p>
                    <?php endif; ?>
                    <hr class="fullwidthline">
                    <div class="facettest">
                    </div>

                </div>
            </div>
            <div class="blockpaddingtop">
                <div class="grid-x grid-margin-x align-center text-center" data-aos="fade-up">
                    <div class="auto cell"></div>
                    <div class="large-8 cell">
                        <div class="altheader ">
                            <h1 class="nbm"><?php the_field('title_programma'); ?></h1>
                        </div>
                    </div>
                    <div class="auto cell" id="target"></div>
                    <?php if (is_front_page()) : ?>
                        <div class="large-12 cell">
                            <a class="arrowlink right ltm" href="/<?php if(isNl()): ?>programma<?php else: ?>programme<?php endif; ?>"><?php pll_e('Naar het programma'); ?> <i class="icon-right"></i></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- DESKTOP LOOP START -->
    <div class="block itemblock  nospace">
        <div class="grid-container full">

            <?php if (is_front_page()) :
                $query = new WP_Query(array('post_type' => 'programma', 'facetwp' => true,  'posts_per_page' => 3));
            else :
                $query = new WP_Query(array('post_type' => 'programma', 'facetwp' => true, 'post__not_in' => array(1764), 'posts_per_page' => -1));
            endif; ?>

            <div class="grid-x grid-margin-x">

               
                <?php $num = 1; ?>
                <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

                        <!-- MOBILE LOOP START -->
                        <?php if ($num < 7) : ?>
                            <!-- MOBILE itemtyp1 START -->
                            <div class="large-12 cell hide-for-large">
                                <div class="colorchangeblock <?php if ($query->current_post % 2 == 0) : echo 'blackbackground';
                                                                else : echo 'whitebackground';
                                                                endif; ?>">
                                    <div class="grid-x ">
                                        <div class="large-12  cell">
                                            <?php the_post_thumbnail('fullwidth', array('class' => 'coverimage')); ?>
                                        </div>

                                        <div class="large-12 cell">

                                            <div class="mobileinner">

                                                <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                <div class="altheader">

                                                    <h3>
                                                        <span>
                                                            <?php echo $taxonomy[0]->name ?>
                                                        </span>
                                                    </h3>
                                                </div>
                                                <hr>
                                                <h2><?php the_title(); ?></h2>

                                                <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                    <ul>
                                                        <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                            <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                        <?php endwhile; ?>
                                                    </ul>
                                                <?php endif; ?>

                                                <a href="<?php the_permalink(); ?>" class="<?php
                                                    if ($query->current_post % 2 == 0) : echo 'whitebutton';
                                                    else : echo '';
                                                    endif; ?> button nbm"><?php pll_e('Lees meer'); ?>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MOBILE itemtyp1 END -->
                        <?php elseif ($num < 12) : ?>
                            <!-- MOBILE itemtyp2 START -->
                            <div class="large-12 cell hide-for-large">
                                <div class="block nieuwsitem hide-for-large">
                                    <div class="grid-container full">
                                        <div class="grid-x grid-margin-x">

                                            <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 1;
                                                                                    else : echo 2;
                                                                                    endif; ?>" data-aos="fade-<?php if ($query->current_post % 2 == 1) : echo 'right';
                                                                                                                else : echo 'left';
                                                                                                                endif; ?>">
                                                <?php if (has_post_thumbnail()) : ?>
                                                    <?php the_post_thumbnail('large', array('class' => 'nieuwsimage')); ?>
                                                <?php else : ?>
                                                    <img class=" nieuwsimage" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                                                <?php endif; ?>
                                            </div>

                                            <div class="small-2 small-order-<?php if ($query->current_post % 2 == 1) : echo 2;
                                                                            else : echo 1;
                                                                            endif; ?> cell">
                                                <!-- EMPTYCELL -->
                                            </div>

                                            <div class="small-10 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 3;
                                                                                    else : echo 4;
                                                                                    endif; ?>">
                                                <div class="mobileinner">
                                                    <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                    <div class="altheader">

                                                        <h3>
                                                            <span>
                                                                <?php echo $taxonomy[0]->name ?>
                                                            </span>
                                                        </h3>
                                                    </div>
                                                    <hr>
                                                    <h2><?php the_title(); ?></h2>

                                                    <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                        <ul>
                                                            <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                                <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                            <?php endwhile; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                    <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('lees meer'); ?> <i class="icon-right"></i></a>

                                                </div>
                                            </div>

                                            <div class="small-2 cell small-order-<?php if ($query->current_post % 2 == 1) : echo 4;
                                                                                    else : echo 3;
                                                                                    endif; ?>">
                                                <!-- EMPTYCELL -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MOBILE itemtyp2 END -->
                        <?php else : ?>
                            <!-- MOBILE itemtyp3 START -->
                            <div class="large-12 cell hide-for-large">
                                <div class="block nieuwsitem hide-for-large">
                                    <div class="grid-container full">
                                        <div class="grid-x grid-margin-x">
                                            <div class="small-1 cell">
                                                <!-- EMPTY -->
                                            </div>
                                            <div class="small-10 cell " data-aos="fade-left">
                                                <?php if (has_post_thumbnail()) : ?>
                                                    <?php the_post_thumbnail('large', array('class' => 'nieuwsimage nieuwsimageflat')); ?>
                                                <?php else : ?>
                                                    <img class="nieuwsimage nieuwsimageflat" src="<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png" alt="default image">
                                                <?php endif; ?>
                                            </div>
                                            <div class="small-1 cell">
                                                <!-- EMPTY -->
                                            </div>
                                            <div class="small-1 cell">
                                                <!-- EMPTY -->
                                            </div>

                                            <div class="small-10 cell ">
                                                <div class="mobileinner">
                                                    <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                    <div class="altheader">

                                                        <h3>
                                                            <span>
                                                                <?php echo $taxonomy[0]->name ?>
                                                            </span>
                                                        </h3>
                                                    </div>
                                                    <hr>
                                                    <h2><?php the_title(); ?></h2>

                                                    <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                        <ul>
                                                            <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                                <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                            <?php endwhile; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                </div>
                                                <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                            </div>

                                            <div class="small-1 cell">
                                                <!-- EMPTY -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MOBILE itemtyp3 END -->
                        <?php endif; ?>
                        <!-- MOBILE LOOP END -->

                        <!-- DESKTOP LOOP START -->

                        <!-- DESKTOP itemtype1 START -->

                        <?php if ($num < 6) : ?>
                            <div class="large-12 cell show-for-large">

                                <div class="hoverblock ">
                                    <div class="grid-x">
                                        <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 1;
                                                                        else : echo 2;
                                                                        endif; ?> cell">
                                            <!-- EMPTYCELL -->
                                        </div>
                                        <div class="large-3 small-order-<?php if ($query->current_post % 2 == 0) : echo 2;
                                                                        else : echo 3;
                                                                        endif; ?> cell">
                                            <div class="flex-container flex-dir-column" style="height: 100%">
                                                <div class="flex-child-grow"></div>
                                                <div class="flex-child-shrink">

                                                    <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                    <div class="altheader">

                                                        <h4>
                                                            <span>
                                                                <?php echo $taxonomy[0]->name ?>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <hr>
                                                    <h2 class="breakword"><?php the_title(); ?></h2>

                                                    <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                        <ul>
                                                            <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                                <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                            <?php endwhile; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                    <a href="<?php the_permalink(); ?>" class="button nbm"><?php pll_e('Lees meer'); ?></a>
                                                </div>
                                                <div class="flex-child-grow"></div>
                                            </div>
                                        </div>
                                        <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 3;
                                                                        else : echo 4;
                                                                        endif; ?> cell">
                                            <!-- EMPTYCELL -->
                                        </div>
                                        <div class="large-5 small-order-<?php if ($query->current_post % 2 == 0) : echo 4;
                                                                        else : echo 1;
                                                                        endif; ?> cell">
                                            <div class="imagecontainer ">
                                                <?php the_post_thumbnail('square', array('class' => 'coverimage')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- DESKTOP itemtype1 END -->
                        <?php elseif ($num < 12) : ?>
                            <!-- DESKTOP itemtype2 START -->
                            <div class="large-12 cell show-for-large">
                                <div class="posttypeblock2 ">
                                    <div class="grid-x grid-margin-x">
                                        <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 0) : echo 1;
                                                                                else : echo 3;
                                                                                endif; ?>">
                                            <!-- EMPTY -->
                                        </div>
                                        <div class="large-4 cell small-order-2">
                                            <div class="flex-container flex-dir-column" style="height: 100%">
                                                <div class="flex-child-grow"></div>
                                                <div class="flex-child-shrink">
                                                    <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                    <div class="altheader">

                                                        <h4>
                                                            <span>
                                                                <?php echo $taxonomy[0]->name ?>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <hr>
                                                    <h2 class="breakword"><?php the_title(); ?></h2>

                                                    <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                        <ul>
                                                            <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                                <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                            <?php endwhile; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                    <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                                </div>
                                                <div class="flex-child-grow"></div>
                                            </div>
                                        </div>
                                        <div class="large-4 cell small-order-<?php if ($query->current_post % 2 == 0) : echo 3;
                                                                                else : echo 1;
                                                                                endif; ?>">
                                            <div class="aos">
                                                <div class="imagecontainer " data-aos="fade-<?php if ($query->current_post % 2 == 0) : echo 'left';
                                                                                            else : echo 'right';
                                                                                            endif; ?>">
                                                    <?php the_post_thumbnail('square', array('class' => 'coverimage')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- DESKTOP itemtype2 END -->
                        <?php else : ?>
                            <!-- DESKTOP itemtype3 START -->
                            <?php if ($query->current_post % 2 == 0) : ?>
                                <div class="large-2 cell show-for-large">
                                    <!-- EMPTY -->
                                </div>
                                <div class="large-4 cell show-for-large">
                                    <div class="posttypeblock3" data-aos="fade-up">
                                        <div class="flex-container flex-dir-column" style="height: 100%">
                                            <div class="flex-child-grow"></div>
                                            <div class="flex-child-shrink">
                                                <div class="imagecontainer ">
                                                    <?php the_post_thumbnail('square', array('class' => 'coverimage')); ?>
                                                </div>
                                                <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                <div class="altheader">

                                                    <h4>
                                                        <span>
                                                            <?php echo $taxonomy[0]->name ?>
                                                        </span>
                                                    </h4>
                                                </div>
                                                <hr>
                                                <h3 class="breakword"><?php the_title(); ?></h3>

                                                <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                    <ul>
                                                        <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                            <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                        <?php endwhile; ?>
                                                    </ul>
                                                <?php endif; ?>
                                                <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                            </div>
                                            <div class="flex-child-grow"></div>
                                        </div>
                                    </div>
                                </div>

                            <?php else : ?>

                                <div class="large-4 cell show-for-large">
                                    <div class="posttypeblock3" data-aos="fade-up">
                                        <div class="flex-container flex-dir-column" style="height: 100%">
                                            <div class="flex-child-grow"></div>
                                            <div class="flex-child-shrink">
                                                <div class="imagecontainer ">
                                                    <?php the_post_thumbnail('square', array('class' => 'coverimage')); ?>
                                                </div>
                                                <?php $taxonomy = get_the_terms(get_the_ID(), 'thema'); ?>
                                                <div class="altheader">

                                                    <h4>
                                                        <span>
                                                            <?php echo $taxonomy[0]->name ?>
                                                        </span>
                                                    </h4>
                                                </div>
                                                <hr>
                                                <h3 class="breakword"><?php the_title(); ?></h3>

                                                <?php if (have_rows('bulletpoints', get_the_ID())) : ?>
                                                    <ul>
                                                        <?php while (have_rows('bulletpoints', get_the_ID())) : the_row(); ?>
                                                            <li><?php the_sub_field('bulletpoint', get_the_ID()); ?></li>
                                                        <?php endwhile; ?>
                                                    </ul>
                                                <?php endif; ?>
                                                <a href="<?php the_permalink(); ?>" class="arrowlink right"><?php pll_e('Lees meer'); ?> <i class="icon-right"></i></a>
                                            </div>
                                            <div class="flex-child-grow"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-2 cell show-for-large">
                                    <!-- EMPTY -->
                                </div>

                            <?php endif; ?>




                            <!-- DESKTOP itemtype3 END -->
                        <?php endif; ?>

                        <!-- DESKTOP LOOP END -->

                        <?php $num++ ?>
                        <!-- RESTART -->
                    <?php endwhile; ?>

                <?php wp_reset_postdata();
                endif; ?>
            </div>
        </div>
    </div>


    <?php if (is_front_page()) : ?>
        <div class="navfooter">
            <div class="grid-container full">
                <div class="grid-x  text-center align-middle">
                    <div class="large-12 cell">
                        <div class="background blackbackground">
                            <a href="/programma" class="arrowlink right"><?php pll_e('naar het volledige programma'); ?> <i class="icon-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>
<!-- PROGRAMMEBLOCK EIND -->
