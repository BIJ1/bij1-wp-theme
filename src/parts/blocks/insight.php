<div class='block insight'>
    <div class='grid-container fluid'>
        <div class='grid-x align-center text-center'>

            <div class='large-8 cell'>

                <?php if (get_field('achtergrond') == 'zwart') : ?>

                    <div class="headercontainer">
                        <?php if (get_field('textsize') == 'groot') : ?>
                            <h1 class="nbm"><?php the_field('titel'); ?></h1>
                        <?php else : ?>
                            <h3 class="nbm"><?php the_field('titel'); ?></h3>
                        <?php endif; ?>
                    </div>

                <?php else : ?>
                    <div class="altheader">
                        <?php if (get_field('textsize') == 'groot') : ?>
                            <h2 class="nbm"><?php the_field('titel'); ?></h2>
                        <?php else : ?>
                            <h3 class="nbm"><?php the_field('titel'); ?></h3>
                        <?php endif; ?>
                    </div>


                <?php endif; ?>
            </div>

        </div>
    </div>
</div>