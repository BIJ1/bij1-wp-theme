<!-- CONTACT START -->

<div class="block contactpage">
    <div class="grid-conainer full">
        <div class="grid-x align-center">
            <div class="large-6 cell">
                <div class="mobileinner">
                    <?php $shortcode = get_field('shortcode'); ?>
                    <?php if (!empty($shortcode)) : ?>

                        <?php echo do_shortcode($shortcode) ?>
                    <?php endif; ?>
                    <?php if (get_field('text')) : ?>
                        <p><small><?php the_field('text'); ?></small> </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT end -->