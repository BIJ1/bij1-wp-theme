<div class="kandidaten">
    <!-- MOBILE LOOP START -->
    <div class="block itemblock  nospace hide-for-large">
        <div class="grid-container full">

            <?php $kandidatennum = 1; ?>
            <?php $query = new WP_Query(array('post_type' => 'kandidaten', 'posts_per_page' => -1)); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="colorchangeblock <?php if ($query->current_post % 2 == 0) : echo 'blackbackground';
                                                    else : echo 'whitebackground';
                                                    endif; ?>">
                        <div class="grid-x ">
                            <div class="large-12  cell">
                                <?php the_post_thumbnail('fullwidth', array('class' => 'coverimage')); ?>
                            </div>
                            <div class="large-12 cell">
                                <div class="mobileinner">

                                    #<?php echo $kandidatennum; ?>
                                    <hr>
                                    <div class="fake altheader">
                                        <span>
                                            <h2 class="nbm"><?php the_title(); ?></h2>
                                        </span>
                                    </div>

                                    <p><?php the_excerpt(); ?></p>
                                    <!--  -->



                                    <a href="<?php the_permalink(); ?>" class="<?php if ($query->current_post % 2 == 0) : echo 'whitebutton';
                                                                                else : echo '';
                                                                                endif; ?> button nbm"> <?php pll_e('Lees meer'); ?></a>


                                </div>
                            </div>

                        </div>
                    </div>

                    <?php $kandidatennum++; ?>
            <?php endwhile;
                wp_reset_postdata();
            endif; ?>
        </div>
    </div>
    <!-- MOBILE LOOP END -->

    <!-- DESKTOP LOOP START -->
    <div class="block itemblock  nospace show-for-large">
        <div class="grid-container full">

            <?php $kandidatennum = 1; ?>
            <?php $query = new WP_Query(array('post_type' => 'kandidaten', 'posts_per_page' => -1)); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="hoverblock">
                        <div class="grid-x">
                            <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 1;
                                                            else : echo 2;
                                                            endif; ?> cell">
                                <!-- EMPTYCELL -->
                            </div>
                            <div class="large-3 small-order-<?php if ($query->current_post % 2 == 0) : echo 2;
                                                            else : echo 3;
                                                            endif; ?> cell">
                                <div class="flex-container flex-dir-column" style="height: 100%">
                                    <div class="flex-child-grow"></div>
                                    <div class="flex-child-shrink">
                                        #<?php echo $kandidatennum; ?>

                                        <hr>

                                        <div class="fake altheader">
                                            <span>
                                                <h2 class="nbm"><?php the_title(); ?></h2>
                                            </span>
                                        </div>

                                        <p><?php the_excerpt(); ?></p>



                                        <a href="<?php the_permalink(); ?>" class="button nbm"><?php pll_e('Lees meer'); ?></a>
                                    </div>
                                    <div class="flex-child-grow"></div>
                                </div>
                            </div>
                            <div class="large-2 small-order-<?php if ($query->current_post % 2 == 0) : echo 3;
                                                            else : echo 4;
                                                            endif; ?> cell">
                                <!-- EMPTYCELL -->
                            </div>
                            <div class="large-5 small-order-<?php if ($query->current_post % 2 == 0) : echo 4;
                                                            else : echo 1;
                                                            endif; ?> cell">
                                <div class="imagecontainer ">
                                    <?php the_post_thumbnail('fullwidth', array('class' => 'coverimage')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $kandidatennum++; ?>
            <?php endwhile;
                wp_reset_postdata();
            endif; ?>
        </div>
    </div>
    <!-- DESKTOP LOOP END -->

</div>