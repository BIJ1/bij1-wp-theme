<?php if (get_field('cover_style') == 'text' or is_singular('vacatures') or is_search()) : ?>
    <!-- TEXTCOVER START -->

    <!-- STICKYNAV START -->
    <div class="stickynav background" role="additional navigation">
        <div class="grid-container fluid">
            <?php if (is_page('programma')) : ?>
                <div class="grid-x">
                    <div class="shrinkcell">
                        <a href="javascript:history.back()" class="button whitebutton arrowbutton nbm"><i class="icon-left"></i><?php pll_e('Terug'); ?></a>
                    </div>
                    <div class="auto cell">
                        <!-- EMPTY -->
                    </div>


                    <?php
                    $file = get_field('programma_file', 'options');
                    if ($file) :
                        $url = wp_get_attachment_url($file); ?>
                        <div class="shrink cell">
                            <a class="button whitebutton" href="<?php echo esc_html($url); ?>">Download pdf</a>
                        </div>
                    <?php endif; ?>

                    <div class="shrink cell">
                        <a class="button whitebutton nbm" id="inhoudsopgavebutton" data-toggle="indexmenu indexmenuoverlay"><?php pll_e('Inhoudsopgave'); ?></a>
                    </div>
                </div>
            <?php else : ?>

                <a href="javascript:history.back()" class="button whitebutton arrowbutton nbm"><i class="icon-left"></i><?php pll_e('Terug'); ?></a>

            <?php endif; ?>
            <hr>
        </div>
    </div>

    <!-- STICKYNAV END -->

    <div class="cover blockpaddingtop" role="text cover">
        <div class="grid-container fluid">
            <div class="grid-x text-center align-center">
                <div class="small-11 large-10 cell">
                    <div class="altheader">
                        <?php if (is_search()) : ?>
                            <?php if (pll_current_language() == 'en') : ?>

                                <h1 class="nbm">This is what we <span>found</span></h1>
                            <?php else : ?>

                                <h1 class="nbm">Dit is wat we <span>hebben</span><span>gevonden</span></h1>
                            <?php endif;  ?>

                        <?php elseif (get_field('title_cover')) : ?>
                            <h1 class="nbm"><?php the_field('title_cover'); ?></h1>
                        <?php else : ?>
                            <?php if (is_singular('vacatures')) : ?>
                                <h1><span><?php the_title(); ?></span></h1>
                            <?php else : ?>
                                <h1><?php the_title(); ?></h1>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- TEXTCOVER END -->
<?php elseif (get_field('cover_style') == 'none' or is_404()) : ?>

    <!-- NO COVER -->
<?php else : ?>
    <!-- IMAGECOVER START -->

    <!-- STICKYNAV START -->
    <?php if (is_front_page()) : ?>
        <!-- frontpage has no sticky navigation -->
    <?php else : ?>
        <div class="stickynav " role="additional navigation">
            <div class="grid-container fluid">


                <?php if (is_singular('kandidaten')) : ?>
                    <div class="grid-x">
                        <div class="shrink cell"> <a href="javascript:history.back()" class="button whitebutton arrowbutton nbm"><i class="icon-left"></i><?php pll_e('Terug'); ?></a>
                        </div>
                        <div class="auto cell"></div>
                        <div class="shrink cell"><a href="/kandidaten/" class="button whitebutton nbm show-for-medium"><?php pll_e('kandidaten overzicht'); ?></a></div>
                        <div class="shrink cell"><a href="/kandidaten/" class="button whitebutton nbm hide-for-medium"><?php pll_e('Kandidaten'); ?></a></div>
                    </div>
                <?php elseif (is_singular('post') or is_singular('programma')) : ?>
                    <a href="javascript:history.back()" class="button whitebutton arrowbutton nbm"><i class="icon-left"></i><?php pll_e('Terug'); ?></a>
                <?php else : ?>
                    <a href="javascript:history.back()" class="button whitebutton arrowbutton nbm"><i class="icon-left"></i><?php pll_e('Terug'); ?></a>
                <?php endif; ?>

            </div>
        </div>
    <?php endif; ?>
    <!-- STICKYNAV END -->

    <?php
        $cover_image = get_field('cover_image');
        $size = 'fullwidth';
        $cover_thumbnail = get_the_post_thumbnail_url(get_the_ID(), $size);
        $thumbnail_alt = str_replace(
            '"',
            "'",
            get_post_meta(get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true )
        );
    ?>

    <div class="cover" role="image cover">
        <?php if (!empty($cover_image)) : ?>
            <div class="imageblock" aria-label="<?php echo $thumbnail_alt; ?>" style="background: url('<?php echo wp_get_attachment_image_url($cover_image, $size); ?>')no-repeat center center / cover;">
            <?php else : ?>

                <?php if (has_post_thumbnail()) : ?>
                    <div class="imageblock" aria-label="<?php echo $thumbnail_alt; ?>" style="background: url('<?php echo $cover_thumbnail; ?>')no-repeat center center / cover;">

                    <?php else : ?>
                        <div class="imageblock" aria-label="BIJ1 logo op regenboogachtergrond" style="background: url('<?php bloginfo('template_url'); ?>/img/includes/bij1_default.png')no-repeat center center / cover;">

                        <?php endif; ?>

                    <?php endif; ?>
                    <div class="inner" data-aos="fade-up">
                        <div class="headercontainer ">
                            <div class="grid-x align-center text-center">
                                <div class="small-11 medium-10 cell">
                                    <?php if (get_field('title_cover')) : ?>
                                        <h1><?php the_field('title_cover'); ?></h1>
                                    <?php else : ?>

                                        <?php if (!empty(get_field('post_title', get_the_ID()))) : ?>
                                            <h1> <?php the_field('post_title', get_the_ID()); ?></h1>
                                        <?php else : ?>
                                            <?php if (is_singular('kandidaten')) : ?>
                                                <h1 class="kandidatenheader"><?php the_title(); ?></h1>
                                            <?php else : ?>
                                                <h1><?php the_title(); ?></h1>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                    <br>

                                    <?php if (get_field('subtitle_cover')) : ?>
                                        <h4><?php the_field('subtitle_cover'); ?></h4>
				    <?php else: ?>
				        <?php bij1_post_published(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php if (is_singular('programma')) : ?>
                            <a href="/<?php if(isNl()): ?>programma<?php else: ?>programme<?php endif; ?>" class="button whitebutton nbm"><?php pll_e('Programma'); ?></a>
                        <?php elseif (is_front_page() && isMainSite()) : ?>
                            <a href="#doneer" class="button whitebutton "><?php pll_e('Doneer'); ?></a><br>
                            <a class="button whitebutton roundbutton nbm" data-smooth-scroll href="#roundbuttonscroll" data-animation-easing="swing" title="scrolldown"><i class="icon-down-open-1"></i></a>
                        <?php else : ?>
                            <a class="button whitebutton roundbutton nbm" data-smooth-scroll href="#roundbuttonscroll" data-animation-easing="swing" title="scrolldown"><i class="icon-down-open-1"></i></a>
                        <?php endif; ?>
                    </div>
                        </div>
                    </div>
                    <div id="roundbuttonscroll"></div>


                    <!-- IMAGECOVER END -->


                <?php endif; ?>
                <!-- IF LUISTER_FILE/LINK -->

                <?php
                $file = get_field('luister_file');
                if ($file) :
                    $url = wp_get_attachment_url($file);
                    $title = get_the_title($file) ?>

                    <div class="block content-block audio">
                        <div class="grid-container">
                            <div class="grid-x align-center text-center">
                                <div class="large-12 cell">
                                    <audio id="myAudioElement" class="hide" src="<?php echo esc_html($url); ?>" controls></audio>
                                    <div class="button roundbutton" id="play" data-toggle="play pause playside pauseside" data-toggler=".hide"><i class="icon-headphones"></i></div>
                                    <div class="button secondary hide roundbutton" id="pause" data-toggle="pause play playside pauseside" data-toggler=".hide"><i class="icon-pause"></i></div>
                                    <p class="nbm"><small><?php echo esc_html($title); ?></small></p>

                                </div>
                            </div>
                        </div>
                    </div>

                <?php else : ?>

                    <?php
                    $link = get_field('luister_link');
                    if ($link) :
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="block content-block audio">
                            <div class="grid-container">
                                <div class="grid-x align-center text-center">
                                    <div class="large-12 cell">
                                        <a href="<?php echo esc_url($link_url); ?>" class="button roundbutton" target="<?php echo esc_attr($link_target); ?>"><i class="icon-headphones"></i></a>
                                        <p class="nbm"><small><?php echo esc_html($link_title); ?></small></p>
                                    </div>

                                </div>
                            </div>
                        </div>

                    <?php endif; ?>
                <?php endif; ?>

                <div class="content">
