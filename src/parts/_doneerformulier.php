<?php if(isMainSite()): ?> 
     <a id="doneer" class="donate-anchor"></a>
     <div class="loading">
         <?php pll_e('Fundraiser wordt geladen'); ?>...

     </div>
     <div class="fundraiser" style="display: none">
         <form class="fundraiser" id="fundraiserWrapper" action="#">
             <div class="grid-x grid-margin-x">
                 <div class="shrink cell text-center">
                     <div class=" fakebutton">
                         <h2 class="nbm">&euro; <span class="raised"></span></h2>
                         <p class="nbm "><small><?php pll_e('is al gedoneerd'); ?></small></p>
                     </div>
                 </div>
                 <div class="auto cell">
                     <!-- EMPTY -->
                 </div>
                 <div class="large-12 cell">
                     <div class="progress nbm" role="progressbar" tabindex="0">
                         <div class="progress-meter"></div>
                     </div>
                 </div>
                 <div class="auto cell">
                     <!-- EMPTY -->
                 </div>
                 <div class="shrink cell">
                     <p><small id="donation-target">&euro;200.000</small></p>
                 </div>
                 <div class="large-12 cell">
                     <p class="nbm"><small><?php pll_e('Ja ik help mee voor'); ?>:</small></p>
                     <hr class="hr alternatehr">
                     <div class="grid-x uitzonderingmargin">
                         <div class="small-4 medium-shrink cell uitzondering">
                             <div class="button expanded whitebutton" data-price="10">&euro;10</div>
                         </div>
                         <div class="small-4 medium-shrink cell uitzondering">
                             <div class="button expanded whitebutton" data-price="25">&euro;25</div>
                         </div>
                         <div class="small-4 medium-shrink cell uitzondering">
                             <div class="button expanded whitebutton" data-price="50">&euro;50</div>
                         </div>
                         <div class="small-12 medium-auto cell uitzondering">
                             <input type="number" class="fakebutton invulbedrag expanded" placeholder="Bedrag" name="amount" id="amount" min="2" />
                         </div>
                     </div>
                     <input type="text" placeholder="Email adres" name="email" id="email" required />
                     <input type="text" placeholder="Voornaam" name="firstName" id="firstName" required />
                     <input type="text" placeholder="Achternaam" name="lastName" id="lastName" required />
                     <div class="donate-extra-fields" style="display: none">
                         <input type="text" placeholder="Straat" name="street" id="street" />
                         <input type="text" placeholder="Postcode" name="zipcode" id="zipcode" />
                         <input type="text" placeholder="Plaats" name="city" id="city" />
                     </div>
                 </div>
                 <div class="shrink cell">
                     <label class="radio">
                         <input type="radio" class="paymentMethod" name="paymentMethod" value="mollie_ideal" checked />
                         <img class="payment-method" src="https://assets.wings.dev/img/icons/ideal.png" alt="iDeal" title="iDeal" />
                     </label>
                 </div>
                 <div class="shrink cell">
                     <label class="radio">
                         <input type="radio" class="paymentMethod" name="paymentMethod" value="mollie_sepabanktransfer" />
                         <img class="payment-method" src="https://assets.wings.dev/img/icons/sepa.png" alt="Sepa overboeking" title="Sepa overboeking" />
                     </label>
                 </div>
                 <div class="auto cell"></div>
                 <div class="large-12 cell">
                     <label class="checkbox" for="newsletter">
                         <input type="checkbox" name="newsletter" id="newsletter" />
                         <?php pll_e('Blijf op de hoogte'); ?>
                     </label>
                 </div>
                 <div class="large-12 cell">
                     <label class="checkbox" for="privacyConsent">
                         <input type="checkbox" name="privacyConsent" id="privacyConsent" required />
                         <?php pll_e('Ik ga akkoord met het'); ?> <a href="https://www.bij1.org/privacy"><?php pll_e('privacybeleid'); ?></a>
                     </label>
                 </div>
             </div>
             <input class="button whitebutton ltm" type="submit" value="Doneer" />
         </form>
     </div>
 <?php endif; ?>
