<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="20" cy="20" r="20" fill="white"/>
<rect x="28.7632" y="31.5913" width="17.2057" height="4.71122" rx="1.70612" transform="rotate(-133.429 28.7632 31.5913)" fill="#161616"/>
<circle cx="17.3423" cy="16.3728" r="8.11238" transform="rotate(-133.429 17.3423 16.3728)" fill="#161616" stroke="white" stroke-width="1.27959"/>
<circle cx="17.3469" cy="16.3664" r="6.92252" transform="rotate(-133.429 17.3469 16.3664)" fill="white" stroke="#161616" stroke-width="1.27959"/>
</svg>
