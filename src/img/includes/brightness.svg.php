<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="20" cy="20" r="20" fill="white"/>
<rect x="4" y="19" width="12.2222" height="1.35" rx="0.675" fill="#161616"/>
<rect x="22.8887" y="19" width="12.2222" height="1.35" rx="0.675" fill="#161616"/>
<rect x="19" y="17" width="12" height="1.35" rx="0.675" transform="rotate(-90 19 17)" fill="#161616"/>
<rect x="19" y="35" width="12.2222" height="1.35" rx="0.675" transform="rotate(-90 19 35)" fill="#161616"/>
<rect x="8.94873" y="8.60791" width="12.2222" height="1.35" rx="0.675" transform="rotate(45 8.94873 8.60791)" fill="#161616"/>
<rect x="22.3052" y="21.9644" width="12.2222" height="1.35" rx="0.675" transform="rotate(45 22.3052 21.9644)" fill="#161616"/>
<rect x="20.7339" y="17.25" width="12.2222" height="1.35" rx="0.675" transform="rotate(-45 20.7339 17.25)" fill="#161616"/>
<rect x="8.16309" y="29.8208" width="12.2222" height="1.35" rx="0.675" transform="rotate(-45 8.16309 29.8208)" fill="#161616"/>
<circle cx="19.6746" cy="20.4363" r="8.07341" fill="white" stroke="white" stroke-width="1.39286"/>
<circle cx="19.5415" cy="20.3033" r="6.96502" fill="white" stroke="#161616" stroke-width="1.28"/>
<path d="M19.5 13C20.4849 13 21.4602 13.1811 22.3701 13.5328C23.2801 13.8846 24.1069 14.4002 24.8033 15.0503C25.4997 15.7003 26.0522 16.4719 26.4291 17.3212C26.806 18.1705 27 19.0807 27 20C27 20.9193 26.806 21.8295 26.4291 22.6788C26.0522 23.5281 25.4997 24.2997 24.8033 24.9497C24.1069 25.5998 23.2801 26.1154 22.3701 26.4672C21.4602 26.8189 20.4849 27 19.5 27L19.5 20L19.5 13Z" fill="#161616"/>
</svg>
