<!DOCTYPE html>
<html class="no-js" lang="nl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
        <?php if (function_exists('is_tag') && is_tag()) {
            echo 'Tag Archive for &quot;' . $tag . '&quot; - ';
        } elseif (is_archive()) {
            wp_title('');
            echo ' Archive - ';
        } elseif (is_search()) {
            echo 'Search for &quot;' . wp_specialchars($s) . '&quot; - ';
        } elseif (!(is_404()) && (is_single()) || (is_page()) and !is_front_page()) {
            wp_title('');
            echo ' - ';
        } elseif (is_404()) {
            echo 'Not Found - ';
        }
        if (is_home() or is_front_page()) {
            bloginfo('name');
            echo ' - ';
            bloginfo('description');
        } else {
            bloginfo('name');
        } ?>
    </title>
    <?php wp_head(); ?>
    <script>
        window.IS_MAIN_SITE = <?php echo (isMainSite() ? 'true' : 'false'); ?>;
    </script>
</head>

<body <?php body_class(); ?>>
    <a href="#skiptocontent" class="skiptocontent">Skip to content</a>

    <!-- LEFTMENU START -->
    <div class="leftmenu" id="leftmenu" data-toggler=".expanded">
        <a role="button" tabindex="1" data-toggle="leftmenu leftmenuoverlay" class="leftmenubutton"><span></span><span></span></a>

        <div class="inner flex-container flex-dir-column">
            <div class="flex-child-shrink">
                <?php
                wp_nav_menu(array(
                  'theme_location'  => 'main',
                  'menu_class'      => 'menu vertical first'
                ));
                ?>
                <?php
                wp_nav_menu(array(
                  'theme_location'  => 'sec_menu',
                  'menu_class'      => 'menu vertical second'
                ));
            ?>
          </div>

        <div class="flex-child-grow"></div>
            <div class="flex-child-shrink">
                <div class="grid-x">
                    <div class="large-12 cell">
                        <h4 class="sbm"><?php pll_e('Ben jij BIJ1'); ?> ?</h4>
                        <?php if (pll_current_language() == 'en') : ?>
                            <p style="margin-bottom: 10px;">Take action and participate <br> for radical equality.</p>
                        <?php else : ?>
                            <p style="margin-bottom: 10px;">Kom in actie en doe mee <br> voor radicale gelijkheid.</p>
                        <?php endif;  ?>
                    </div>
                </div>

                <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'cta_menu',
                        'menu_class'      => 'menu vertical ctamenu'
                    ));
                ?>
            </div>
        </div>
    </div>

    <a href="" class="overlay" id="leftmenuoverlay" data-toggler=".expanded" data-toggle="leftmenu leftmenuoverlay"></a>
    <!-- LEFTMENU END -->

    <!-- TOPBARSTART -->
    <div class="topbar" role="navigation">
        <div class="fluid grid-container">
            <div class="grid-x align-middle">
                <div class="auto cell">
                    <div class="grid-x align-middle">
                        <div class="shrink cell">
                            <button style="cursor: pointer" data-toggle="leftmenu leftmenuoverlay" id="menu-toggle" tabindex="1"><?php get_template_part('img/includes/menu.svg'); ?></button>
                        </div>
                        <div class="auto cell show-for-large">
                            <?php
                                wp_nav_menu(array(
                                    'theme_location'  => 'header',
                                    'menu_class'      => 'menu'
                                ));
                            ?>
                        </div>
                    </div>
                </div>

                <div class="shrink cell">
                    <a class="logo" href="<?php echo site_url(); ?>" title="home">
                        <img src="<?php echo logoBigPath(); ?>" class="big" alt="BIJ1" />
                        <img src="<?php echo logoSmallPath(); ?>" class="small" alt="BIJ1" />
                    </a>
                </div>

                <div class="auto cell text-right">
                    <div class="grid-x align-middle">
                        <div class="auto cell">
                            <ul class="menu small-padding align-right">
                                <li class="hide-for-medium"> <a id="iconmenubutton" data-toggle="iconmenubutton iconmenu" data-toggler=".expanded"><span></span><span></span></a></li>
                                <?php
                                    wp_nav_menu(array(
                                        'theme_location'  => 'language_menu',
                                        'menu_class'      => 'languageswitch show-for-medium'
                                    ));
                                ?>
                                <li class="show-for-medium"><a><button class="aftercolor search-button" data-toggle="searchmenu" alt="Search menu" style="cursor: pointer"><?php get_template_part('img/includes/search.svg'); ?></button></a></li>
                            </ul>
                        </div>

                        <div class="shrink cell">
                            <?php if(isMainSite()): ?>
                            <a href="https://bij1.org/#doneer" class="nbm secondary button roundbutton hide-for-medium"><?php get_template_part('img/includes/donatemobile.svg'); ?></a>
                            <a href="https://bij1.org/#doneer" class="nbm secondary button show-for-medium"><?php pll_e('Doneer'); ?></a>
                            <?php else: ?>
                            <a href="https://bij1.org/#doneer" target="_blank" class="nbm secondary button roundbutton hide-for-medium"><?php get_template_part('img/includes/donatemobile.svg'); ?></a>
                            <a href="https://bij1.org/#doneer" target="_blank" class="nbm secondary button show-for-medium"><?php pll_e('Doneer'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- COLORBAR START -->
        <div class="full grid-container">
            <div class="grid-x border">
                <div class="small-2 cell">
                    <div class="line orange"></div>
                </div>
                <div class="small-2 cell">
                    <div class="line pink"></div>
                </div>
                <div class="small-2 cell">
                    <div class="line green"></div>
                </div>
                <div class="small-2 cell">
                    <div class="line purple"></div>
                </div>
                <div class="small-2 cell">
                    <div class="line red"></div>
                </div>
                <div class="small-2 cell">
                    <div class="line blue"></div>
                </div>
            </div>
        </div>
        <!-- COLORBAR END -->
    </div>
    <!-- TOPBAR END -->

  
    <!-- ICONMENU START -->
    <div id="iconmenu" data-toggler=".expanded">
        <div class="grid-x">
            <div class="auto cell"></div>
            <div class="shrink cell">
                <div class="button fakebutton whitebutton">
                    <ul class="menu ">
                        <?php
                            wp_nav_menu(array(
                                'theme_location'  => 'language_menu',
                                'menu_class'      => 'languageswitch'
                            ));
                        ?>
                        <li class=""><a class="5padding" data-toggle="searchmenu iconmenu"><?php get_template_part('img/includes/search.svg'); ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="auto cell"></div>
        </div>
    </div>
    <!-- ICONMENU END -->

    <?php if(is_page( 'programma' )):?>
        <!-- INDEXMENU START -->

        <div id="indexmenu" class="testjs" data-toggler=".expanded">
            <a class="indexclosebutton" data-toggle="indexmenu indexmenuoverlay" data-toggler=".expanded"><span></span><span></span></a>
            <div class="grid-container fluid">
                <hr>
                    <div class="grid-x align-center text-center">
                        <div class="small-12 large-8 cell">
                            <div class="altheader">
                                <h4><span><?php pll_e('Inhoudsopgave'); ?></span></h4>
                                <div class="grid-x grid-margin-x align-center">
                                <div class="shrink cell">
                                    <!-- TODO, change static permalink -->
                                    <a href="/programma" class="arrowlink right"><?php pll_e('Introductie'); ?> <i class="icon-right"></i></a>
                                </div>

                                <?php
                                    $link = get_sub_field('pagina_link', 'options');
                                    if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                        <div class="shrink cell">
                                            <a class="arrowlink right" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?> <i class="icon-right"></i></a>

                                        </div>
                                    <?php endif; ?>

                                <div class="large-12 cell">
                                    <div class="inner">
                                        <?php echo do_shortcode('[facetwp facet="themas"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="overlay" id="indexmenuoverlay" data-toggler=".expanded" data-toggle="indexmenu indexmenuoverlay"></a>
        <!-- INDEXMENU END -->
    <?php endif;?>

    <!-- ROUNDBUTTON CTA START-->
    <a href="https://doemee.bij1.org/" target="_blank" class="doemee">
        <h2 class="nbm">DOE<br>MEE</h2>
    </a>

    <div class="container">
        <?php get_template_part('parts/_cover') ?>
        <!-- ROUNDBUTTON CTA END-->

        <!-- SIDEBAR POST -->
        <?php if (is_singular('post') or is_singular('programma') or is_page('programma')) : ?>
            <div class="sidebarbuttons" data-aos="fade-right">
                <div class="menu vertical">

                <?php
                    $file = get_field('luister_file');
                    if ($file) :
                        $url = wp_get_attachment_url($file);
                        $title = get_the_title($file) ?>
                        <audio id="myAudioElement" class="hide" src="<?php echo esc_html($url); ?>" controls></audio>
                        <div class="button roundbutton whitebutton" id="playside" data-toggle="play pause playside pauseside" data-toggler=".hide"><i class="icon-headphones"></i></div>
                        <div class="button secondary hide roundbutton" id="pauseside" data-toggle="pause play playside pauseside" data-toggler=".hide"><i class="icon-pause"></i></div>
                    <?php endif; ?>

                    <?php if (is_singular('post') or is_singular('programma')) : ?>
                        <a href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="button roundbutton whitebutton"><i class="icon-facebook"></i></a>
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class=" button roundbutton whitebutton" data-show-count="false"><i class="icon-twitter"></i></a>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    <div id="skiptocontent"></div>
