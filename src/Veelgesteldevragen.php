<?php /* Template Name: Veelgestelde vragen */ ?>


<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


        <div class="block faq">
            <div class="grid-container fluid">
                <div class="grid-x align-center">
                    <div class="large-8 cell">
                        <?php if (have_rows('vragen')) : ?>
                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                <?php while (have_rows('vragen')) : the_row(); ?>
                                    <li class="accordion-item" data-accordion-item>
                                        <a href="#" class="accordion-title"><?php the_sub_field('vraag'); ?></a>

                                        <div class="accordion-content" data-tab-content>
                                            <p class="nbm"><?php the_sub_field('antwoord'); ?></p>
                                        </div>

                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


<?php endwhile;
endif; ?>

<?php get_footer(); ?>