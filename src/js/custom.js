/**
 * Customizations by Jelle
 */

(function(){

$(document).ready(function(){

    $('a.menu-toggle').click(function(){
        if($('div.top-menu nav').hasClass('toggled')){
            $('div.top-menu nav').slideUp(400, function(){
                $('div.top-menu nav').removeClass('toggled');
                $('div.top-menu nav').attr('style', '');
            });
            $('a.menu-toggle').html('&#9776;');
        } else {
            $('div.top-menu nav').slideDown(400, function(){
                $('div.top-menu nav').addClass('toggled');
                $('div.top-menu nav').attr('style', '');
            });
            $('a.menu-toggle').html('&#10005;');
        }
    });

    $('div.top-menu nav ul.menu > li a').on('click', function(){
        var el = $(this).siblings('.sub-menu');

        if(el.hasClass('focused')) {
            el.removeClass('focused').attr('aria-expanded', 'false');
        } else {
            $('div.top-menu nav ul.sub-menu').removeClass('focused').attr('aria-expanded', 'false');
            el.addClass('focused').attr('aria-expanded', 'true');
        }
    });

});

})();
