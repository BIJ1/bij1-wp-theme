$(document).foundation();

$(document).ready(function () {
  'use strict';
   var c, currentScrollTop = 0,
       navbar = $('.stickynav');
       $(window).scroll(function () {
          var a = $(window).scrollTop();
          var b = navbar.height();
          currentScrollTop = a;
          if (c < currentScrollTop && a > b + b) {
            navbar.addClass("scrollUp");
          } else if (c > currentScrollTop && !(a <= b)) {
            navbar.removeClass("scrollUp");
          }
          c = currentScrollTop;
    });
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 150) {
                $('.stickynav').addClass('scroll');
            } else {
                $('.stickynav').removeClass('scroll');
            }
        });
    });    
});

$( ".hoverblock" ).hover(
  function() {
    $( this ).find( ".large-5" ).addClass('large-7').removeClass('large-5');
    $( this ).find( ".large-2" ).addClass('large-1').removeClass('large-2');
  }, function() {
    $( this ).find( ".large-7" ).addClass('large-5').removeClass('large-7');
    $( this ).find( ".large-1" ).addClass('large-2').removeClass('large-1');
  }
);

/* PLAYPAUSE BUTTON */
$('#play').click(function() {
    $("#myAudioElement")[0].play();
});

$('#pause').click(function() {
    $("#myAudioElement")[0].pause();
});

$('#playside').click(function() {
    $("#myAudioElement")[0].play();
});

$('#pauseside').click(function() {
    $("#myAudioElement")[0].pause();
});



/* FUNDRAISER */
const fundraiserWrapper = document.querySelector('#fundraiserWrapper');
(function(){
    if(!window.IS_MAIN_SITE) {
        return;
    }

    var FUNDRAISER_ID = 'n04pD9zVB4f4vXE0NO28';
    var AUTHORIZATION_KEY = 'z4SS8rqLDrNWMChzslf3IBuWJG1ldbSy';
    var WINGS_APP_DOMAIN = 'www.bij1.org';


    var switchAmount = function(){
        var amount = parseInt($('form.fundraiser input.invulbedrag').val());
        if(amount >= 500) {
            $('div.donate-extra-fields').show();
            $('div.donate-extra-fields input').attr('required', true);
        } else {
            $('div.donate-extra-fields').hide();
            $('div.donate-extra-fields input').attr('required', false);
        }
    };

    $(document).ready(function(){
        // Haal de gegevens van de fundraiser op, en toon het formulier en de
        // huidige stand als het is opgehaald
        retrieveFundraiser(
            FUNDRAISER_ID,
            AUTHORIZATION_KEY,
        );

        $('form.fundraiser input.invulbedrag').on('change', switchAmount);
        $('form.fundraiser input.invulbedrag').on('keyup', switchAmount);
        $('form.fundraiser input.invulbedrag').on('paste', switchAmount);
        switchAmount();

        // Verstuur het donatieformulier
        $('form.fundraiser').submit(function(event){
            event.preventDefault();
            event.stopPropagation();

            // NB: Donatiehoeveelheden worden geteld in centen!
            var amount = parseInt($('input#amount').val()) * 100;
            var firstName = $('input#firstName').val();
            var lastName = $('input#lastName').val();
            var email = $('input#email').val();
            var street = $('input#street').val();
            var zipcode = $('input#zipcode').val();
            var city = $('input#city').val();
            var newsletter = $('input#newsletter').is(':checked');
            var privacyConsent = $('input#privacyConsent').is(':checked');
            var paymentMethod = $("input.paymentMethod:checked").val();

            var ajaxSettings = {
                contentType: 'application/json',
                data: submitFundraiserMutation(
                    FUNDRAISER_ID,
                    amount,
                    email,
                    firstName,
                    lastName,
                    privacyConsent,
                    newsletter,
                    paymentMethod,
                    street,
                    zipcode,
                    city
                ),
                dataType: 'json',
                error: submitError,
                headers: {
                    'Authorization': 'Bearer ' + AUTHORIZATION_KEY,
                    'X-Wings-App-Domain': WINGS_APP_DOMAIN,
                },
                success: submitSuccess,
                url: 'https://api.wings.dev/',
            };

            $.post(ajaxSettings);

        });
    });

    // Haal de gegevens van de fundraiser op via de API van wings
    var retrieveFundraiser = function(fundraiserId, authorization){
        var ajaxSettings = {
            contentType: 'application/json',
            data: getFundraiserQuery(fundraiserId),
            dataType: 'json',
            error: retrieveError,
            headers: {
                'Authorization': 'Bearer ' + authorization,
                'X-Wings-App-Domain': WINGS_APP_DOMAIN,
            },
            success: setupForm,
            url: 'https://api.wings.dev/',
        };

        $.post(ajaxSettings);
    };

    function formatMoney(number, decPlaces, decSep, thouSep) {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSep = typeof decSep === "undefined" ? "." : decSep;
        thouSep = typeof thouSep === "undefined" ? "," : thouSep;
        var sign = number < 0 ? "-" : "";
        var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
        var j = (j = i.length) > 3 ? j % 3 : 0;

        return sign +
            (j ? i.substr(0, j) + thouSep : "") +
            i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
            (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
    }

    // Toon het donatieformulier, wordt aangeroepen als callback van
    // retrieveFundraiser
    var setupForm = function(data){
        var campaign = data['data']['campaign'];
        var raised = campaign['raised']['amount'] / 100;
        $('span.raised').html(raised);

        var targetStepSize = 10000;
        if(raised > 10000) targetStepSize = 100000;
        var target = 10000;

        for(var i = 10000000; i >= 100000; i = i - 100000){
            if(raised >= i) {
                break;
            }
            target = i;
        }

        for(var i = 100000; i >= 10000; i = i - 10000){
            if(raised >= i) {
                break;
            }
            target = i;
        }

        if(target < 50000) {
            target = 50000;
        }

        var amount = raised;
        var new_amount = amount * 100;
        var new_amount = new_amount / target;
        $('small#donation-target').html('&euro;' + formatMoney(target, 0, ',', '.'));
        $('.progress-meter').css('width', new_amount + "%");
          // progressbar width


        /* buttonprices */
        const priceButtons = document.querySelectorAll('div[data-price]');
        const fakebutton = document.querySelector('.invulbedrag');
        let clickedPrice;
        priceButtons.forEach(button => button.addEventListener('click', setPrice));

        function setPrice(e) {
            clickedPrice = e.currentTarget.dataset.price;
            console.log(clickedPrice);
            fakebutton.value = clickedPrice;
            switchAmount();
        }

        $('div.loading').hide();
        $('div.fundraiser').fadeIn();
    };

    // Errorboodschap als de fundraiser niet opgehaald kon worden via de API van Wings
    var retrieveError = function(){
        //alert('Er is iets mis gegaan met het ophalen van het donatieformulier.');
    };

    // Errorboodschappen als het submitten van het donatieformulier niet is geslaagd.
    var submitError = function(){
        alert('Er is iets mis gegaan met het verwerken van uw donatie.'); 
    };

    var submitSuccess = function(data){
        if(data.hasOwnProperty('errors') && data.errors.length > 0){
            console.log(data);
            return submitError();
        }

        // Als we een payment url terug hebben gekregen, dan redirecten we de
        // gebruiker naar die url zodat ze de betaling via Mollie rond kunnen
        // maken
        if(data['data'] && data['data']['submitFundraiser']
            && data['data']['submitFundraiser']['donation']
            && data['data']['submitFundraiser']['donation']['order']
            && data['data']['submitFundraiser']['donation']['order']['paymentUrl']
        ){
            window.location = data['data']['submitFundraiser']['donation']['order']['paymentUrl'];
        } else {
            console.log(data);
            return submitError();
        }
    };

    // Stel de query op voor het ophalen van de fundraiser met het gegeven id
    var getFundraiserQuery = function(id){
        return JSON.stringify({
            "query": "\n  query ($selector: FundraiserSelectorInput) {\n    campaign: fundraiser(selector: $selector) {\n      id\n      title\n      submissionSchema\n      settings {\n        legal {\n          terms {\n            url\n          }\n          privacyPolicy {\n            url\n          }\n        }\n      }\n      target {\n        amount\n        currency {\n          id\n          name\n          symbol\n        }\n      }\n      amounts {\n        options {\n          amount {\n            amount\n            currency {\n              id\n              name\n              symbol\n            }\n          }\n        }\n      }\n      raised {\n        amount\n        currency {\n          id\n          name\n          symbol\n        }\n      }\n      paymentMethods {\n        id\n        title\n        icons {\n          url\n        }\n      }\n      ...NodeFields\n      ...CampaignFields\n    }\n  }\n\n  fragment NodeFields on Node {\n    id\n    title\n    resourceType\n    slug\n    locale {\n      id\n      name\n      primary\n    }\n    image {\n      id\n      name\n      caption\n      alt\n      key\n      url\n    }\n    meta {\n      key\n      value\n    }\n    data {\n      key\n      data\n    }\n    status\n    nodeType\n    copy {\n      message {\n        messageId\n        description\n        message\n      }\n    }\n  }\n\n  fragment CampaignFields on Campaign {\n    intro\n    description\n  }\n",
            "variables": {
                "selector": {
                    "id":{
                        "eq": id
                    }
                }
            }
        });
    };

    // Stel de mutation op voor het indienen van een donatie aan de API
    var submitFundraiserMutation = function(
        id, amount, email, firstName, lastName,
        privacyConsent, newsletter, paymentMethod,
        street, zipcode, city
    ){
        if(street === undefined){
            street = '';
        }

        if(zipcode === undefined){
            zipcode = '';
        }

        if(city === undefined){
            city = '';
        }

        newsletter = newsletter === true;
        privacyConsent = privacyConsent === true;

        var data = {
            'email': email,
            'firstName': firstName,
            'lastName': lastName,
            'privacyConsent': privacyConsent,
            'newsletter': newsletter,
            'Q8m5C4STw2jmMfyq2': street,
            'QoUY33RTcR86wh82a': zipcode,
            'tIz9b5AwfZTVBgr8o': city,
        };

        return JSON.stringify({
            "query":"\n  mutation SubmitFundraiser($input: SubmitFundraiserInput!) {\n    submitFundraiser(input: $input) {\n      donation {\n        id\n        order {\n          id\n          paymentUrl\n        }\n      }\n    }\n  }\n",
            "variables": {
                "input":{
                    "id": FUNDRAISER_ID,
                    "data": JSON.stringify(data),
                    "amount": amount,
                    "paymentMethod": paymentMethod,
                    "redirectUrl": "https://www.bij1.org/bedankt"
                }
            }
        });
    };
})();




/* INHOUDSOPGAVE FACET FIX */
$('.facetwp-facet').click(function() { 
    $('#indexmenu').removeClass('expanded'); 
    $('#indexmenuoverlay').removeClass('expanded'); 
    $('html, body').animate({
        scrollTop: $("#target").offset().top
    }, 500);
});

$('.targetclass').click(function() { 
    $('html, body').animate({
        scrollTop: $("#target").offset().top
    }, 500);
});


/* AOS */



AOS.init();

// You can also pass an optional settings object
// below listed default settings
AOS.init({
  // Global settings:
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 1000, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: false, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
});
