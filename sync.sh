#!/bin/bash
SSH_USER_PROD="${SSH_USER_PROD:-root}"
# intro
THEME_DIR=/var/www/wordpress/wp-content/themes/BIJ1/
BRANCH=`git symbolic-ref --short HEAD`
# sync staging
SSH_HOST=wp-staging.bij1.net
ACTUAL_BRANCH=staging-actual
SSH_USER=root
git checkout main
git branch -D $ACTUAL_BRANCH
git checkout -b $ACTUAL_BRANCH
rsync -rauL $SSH_USER@$SSH_HOST:$THEME_DIR/* ./src
git add .
git commit -m "sync from staging server"
git push --force
# sync production
SSH_HOST=wp.bij1.net
ACTUAL_BRANCH=production-actual
SSH_USER=$SSH_USER_PROD
git checkout main
git branch -D $ACTUAL_BRANCH
git checkout -b $ACTUAL_BRANCH
rsync -rauL $SSH_USER@$SSH_HOST:$THEME_DIR/* ./src
git add .
git commit -m "sync from production server"
git push --force
# outro
git checkout $BRANCH
